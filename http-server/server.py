import http.server
import cgi

PORT = 8888
server_addr = ("", PORT)
index = open("./public/index.html", "r")

def initServer():
    server = http.server.HTTPServer
    handler = http.server.CGIHTTPRequestHandler
    handler.cgi_directories = ['public']
    print("Server launched at", PORT, "port.")
    
    httpd = server(server_addr, handler)
    httpd.serve_forever()
    